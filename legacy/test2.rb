require 'state'
require 'set'
require 'test/unit'

class TC_State < Test::Unit::TestCase
	include UpState

	Tony = State.new_type("tony", [Event.new("tonyup", {:tony => "tony"})], [], [:tony])
	Mike = State.new_type("mike", [], [Dependency.new(Tony)])
	ChainTop = State.new_type("chaintop", [], [Dependency.new(Tony, {:tony => "tony"}, {:tony => :bob}),
											   Dependency.new(Mike)])
	#this state should never appear
	Reversed = State.new_type("reversed", [Dependency.new(Tony)], [Event.new("tonyup", {:tony => "tony"})])

	def put_states
		foo = State.send(:class_variable_get, :@@states).map{ |x| x.to_s_color }
		puts foo.sort.join(", ")
	end

	def assert_have_states(*s)
		assert_equal(s.sort, State.get_all.map{ |x| x.to_s }.sort)
	end

	def assert_only_states(*s)
		assert_have_states *s
		assert_equal(State.get_all.size, s.size)
	end

	def setup
		assert_only_states(
			'TonyState{} (down)'
		)
	end

	def teardown
		State.release(:user)
		State.release(:system)
	end

	def test_reversed
		#this should never ever come up
		Reversed.hold(:user)
		assert_only_states(
			'TonyState{} (down)'
		)
	end

	def test_incomplete_hold
		begin
			Tony.hold(:user)
			assert(false)
		end
		rescue AmbiguousRequest => e
			assert(true)
	end

	def test_bogus_hold
		begin
			Tony.hold(:cookies)
			assert(false)
		end
		rescue TypeError => e
			assert(true)
	end

	def test_bogus_release
		begin
			State.process_event(Event.new("tonyup", {:tony => "tony"}))
			Tony.release(:cookies)
			assert(false)
		end
		rescue TypeError => e
			assert(true)
	end

	def test_chained_dependencies
		State.process_event(Event.new("tonyup", {:tony => "tony"}))
		Mike.hold(:user)
		ChainTop.hold(:system, {:bob => "tony"})
		assert_only_states(
			'TonyState{} (down)',
			'ChaintopState{:bob=>"tony"} (up)',
			'MikeState{} (up)',
			'TonyState{:tony=>"tony"} (up)'
		)
	end

	def test_chained_releases
		test_chained_dependencies
		ChainTop.release(:system)
		Tony.release(:system)
	end
end
