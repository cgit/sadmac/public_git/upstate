require 'state'

include UpState

HdAvail = State.new_type("hdAvail", [Event.new("DevKit_FoundHD")], [], [:uid, :name, :blkdev])
FstabAvail = State.new_type("fstabLine", [Event.new("Can_Mount", {:mount_str => /.*/})], [], [[:uid, :name, :blkdev], :mount_str])
Mount = State.new_type("mount", [Event::Epsilon], [Dependency.new(HdAvail, {:uid => /.*/}), Dependency.new(FstabAvail, {:mount_str => /.*/})])

events = [
	["DevKit_FoundHD", {:uid => "1234", :name => "myhd", :blkdev => "/dev/sda1"}],
	["Can_Mount", {:uid => "1234", :mount_str => "/home"}],
	["Can_Mount", {:uid => "1234"}],
]

State.print_all_color
events.each do |x|
	State.process_event Event.new(*x)
	State.print_all_color
end
