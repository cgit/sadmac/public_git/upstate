def cross(first, *args):
    """
    find cross product of two sets.
    """
    retval = set([ (x,) for x in first ])
    for arg in args:
        next = set()
        for a in retval:
            for b in arg:
                next.add(a + (b,))
        retval = next
    return retval
